import { Col, Container, Input, Label, Row, Button } from "reactstrap";
import '../App.css'
import imageavatar from '../assets/img/avatar.png'

function FormEmployee () {
    return(
       <Container style={{width:'1000px'}} className="bg-light text-center mt-5">
            <Row>
                <Col className="mt-2">
                    <h1>Employee Form</h1>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Row className="mt-2">
                        <Col sm={3}>
                            <Label>Họ và tên</Label>
                        </Col>
                        <Col>
                            <Input></Input>
                        </Col>
                    </Row>
                    <Row xs={12} className="mt-2">
                        <Col sm={3}>
                            <Label>Ngày sinh</Label>
                        </Col>
                        <Col>
                            <Input></Input>
                        </Col>
                    </Row>
                    <Row xs={12} className="mt-2">
                        <Col sm={3}>
                            <Label>Số điện thoại</Label>
                        </Col>
                        <Col>
                            <Input></Input>
                        </Col>
                    </Row>
                    <Row xs={12} className="mt-2">
                        <Col sm={3}>
                            <Label>Giới tính</Label>
                        </Col>
                        <Col>
                            <Input></Input>
                        </Col>
                    </Row>
                </Col>
                <Col>
                    <img src={imageavatar} alt="avatar" className="img-thumbnail" width={'200px'}/>
                </Col>
            </Row>
            <Row className="mt-2">
                <div className="col-sm-2">
                    <p>Công việc</p>
                </div>
                <Col>
                    <Input type="textarea"></Input>
                </Col>
            </Row>
            <Row>
                <Col className="mt-3 mb-5">
                    <Button className="mt-2 mr-2" color="info" >Chi tiết</Button> {' '}
                    <Button className="mt-2" color="danger">Kiểm tra</Button>
                </Col>
            </Row>
       </Container>
    )
}

export default FormEmployee;